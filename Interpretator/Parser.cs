﻿using System.Collections.Generic;
using System.Linq;
using Interpretator.Expressions;

namespace Interpretator
{
    class Parser
    {
        private Stack<IExpression> OutputQueue { get; set; }
        private Stack<Token> OpStack { get; set; }

        public Parser()
        {
            OutputQueue = new Stack<IExpression>();
            OpStack = new Stack<Token>();
        }

        public void formOperators(List<Token> tokenList)
        {
            for(int i = 0, n = tokenList.Count; i < n; i++)
            {
                    switch (tokenList[i].ExprType)
                    {
                        case NumberExpression ex2:
                            OutputQueue.Push(tokenList[i].ExprType);
                            break;
                        case VariableExpression ex2:
                            OutputQueue.Push(tokenList[i].ExprType);
                            break;
                        case BinaryExpression ex2:
                            while(OpStack.Count > 0)
                            {
                                if(!(OpStack.Peek().ExprType is BinaryExpression))
                                {
                                    break;
                                }
                                if(tokenList[i].Priority <= OpStack.Peek().Priority)
                                {
                                    OutputQueue.Push(OpStack.Pop().ExprType);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            OpStack.Push(tokenList[i]);
                            break;
                        case UnaryExpression ex2:
                            while (OpStack.Count > 0)
                            {
                                if (!(OpStack.Peek().ExprType is BinaryExpression))
                                {
                                    break;
                                }
                                if (tokenList[i].Priority <= OpStack.Peek().Priority)
                                {
                                    OutputQueue.Push(OpStack.Pop().ExprType);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            OpStack.Push(tokenList[i]);
                            break;
                        case LBracketExpression ex2:
                            OpStack.Push(tokenList[i]);
                            break;
                        case RBracketExpression ex2:
                            while(!(OpStack.Peek().ExprType is LBracketExpression) && OpStack.Count > 0)
                            {
                                OutputQueue.Push(OpStack.Pop().ExprType);
                                if (OpStack.Peek().Equals(null))
                                {
                                    //todo No charachter Exception
                                }
                            }
                            if(OpStack.Peek().ExprType is LBracketExpression)
                            {
                                OpStack.Pop();
                            }
                            break;
                        default:
                            OpStack.Push(tokenList[i]);
                            break;
                    }
            }
                while (OpStack.Count != 0)
                {
                    OutputQueue.Push(OpStack.Pop().ExprType);
                }
        }



        public IExpression buildTree()
        {
            IExpression root = OutputQueue.Pop();
            checkIfExpr(ref root);
            addChild(root);
            return root;
        }

        public IExpression addChild(IExpression node)
        {
            switch (node)
            {
                case BinaryExpression ex:
                    if (ex is IFucntionExpression)
                    {
                        ex.expr2 = OutputQueue.Pop();
                        addChild(ex.expr2);
                        if (OutputQueue.Count == 0 || OutputQueue.Peek() == null)
                        {
                            ex.expr1 = ex.expr2;
                            addChild(ex.expr1);
                        }
                        else
                        {
                            ex.expr1 = OutputQueue.Pop();
                            addChild(ex.expr1);
                        }
                    }
                    else
                    {
                        ex.expr2 = OutputQueue.Pop();
                        addChild(ex.expr2);
                        if(OutputQueue.Count == 0)
                        {
                            throw new System.Exception("Binary expression should have 2 arguments");
                        }
                        ex.expr1 = OutputQueue.Pop();
                        addChild(ex.expr1);
                    }
                    break;
                case UnaryExpression ex1:
                    if (OutputQueue.Count == 0)
                    {
                        throw new System.Exception("Unary expression should have 1 argument");
                    }
                    ex1.expr1 = OutputQueue.Pop();
                    addChild(ex1.expr1);
                    break;
                default:
                    break;
            }
            return node;
        }

        public void checkIfExpr(ref IExpression root)
        {
            if (root is IfExpression)
            {
                IfExpression tempIf = new IfExpression();
                tempIf.condition = OutputQueue.Pop();
                addChild(tempIf.condition);
                root = tempIf;
            }
            if (root is WhileExpression)
            {
                WhileExpression tempWhile = new WhileExpression();
                tempWhile.condition = OutputQueue.Pop();
                addChild(tempWhile.condition);
                root = tempWhile;
            }
        }
    }

}
