﻿using Interpretator.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator
{
    public class Token
    {
        public int Priority { get; set; }
        public IExpression ExprType { get; set; }
    }
}
