﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public class AssignmentExpression : BinaryExpression
    {

        public override object Eval(Context context)
        {
            expr1.Eval(context);
            VariableExpression variable = expr1 as VariableExpression;
            object value = Convert.ToDouble(expr2.Eval(context));
            context.SetVariable(variable.name, Convert.ToDouble(value));
            return Convert.ToDouble(value);
        }
    }
}
