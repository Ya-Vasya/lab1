﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Interpretator.Expressions
{
    class LeftClick : IExpression
    {

        public object Eval(Context context)
        {
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftDown);
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftUp);
            return " " + '\b';
        }

    }
}
