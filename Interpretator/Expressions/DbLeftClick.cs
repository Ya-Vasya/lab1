﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions
{
    class DbLeftClick : IExpression
    {

        public object Eval(Context context)
        {
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftDown);
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftUp);
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftDown);
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftUp);
            return " " + '\b';
        }
    }
}
