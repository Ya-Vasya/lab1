﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public class MultiplyExpression : BinaryExpression
    {
        public override object Eval(Context context)
        {
            return Convert.ToDouble(expr1.Eval(context)) * Convert.ToDouble(expr2.Eval(context));

        }
    }
}
