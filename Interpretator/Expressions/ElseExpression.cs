﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions
{
    class ElseExpression : IExpression
    {
        public IExpression elseBlock { get; set; }

        public ElseExpression()
        {
            elseBlock = null;
        }

        public object Eval(Context context)
        {
            if (elseBlock is BlockExpression == false)
                throw (new Exception("'else' operator without expression."));
            return elseBlock.Eval(context);
        }
    }
}
