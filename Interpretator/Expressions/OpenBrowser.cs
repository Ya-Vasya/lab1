﻿namespace Interpretator.Expressions
{
    class OpenBrowser : IExpression
    {
        public object Eval(Context context)
        {
            System.Diagnostics.Process.Start("http://google.com");
            return "";
        }
    }
}
