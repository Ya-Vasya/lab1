﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Runtime.InteropServices;

namespace Interpretator.Expressions
{
    class MoveMouse : BinaryExpression, IFucntionExpression
    {
        public override object Eval(Context context)
        {
            int xPos = Convert.ToInt32(expr1.Eval(context)), yPos = Convert.ToInt32(expr2.Eval(context));
            MouseEvents.SetCursorPosition(xPos, yPos);
            return " " + '\b';
        }
    }
}
