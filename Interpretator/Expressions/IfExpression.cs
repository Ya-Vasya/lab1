﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions
{
    class IfExpression : ConditionalExpression, IExpression
    {
        public IExpression negativeBlock { get; set; }

        public IfExpression()
        {
            condition = null;
            negativeBlock = null;
            positiveBlock = null;
        }

        public object Eval(Context context)
        {
            if (positiveBlock is BlockExpression == false)
            {
                throw (new Exception("'if' operator without expression"));
            }
            object conditionResult = condition.Eval(context);
            int r = 0;
            if (conditionResult != null)
                if (conditionResult is int result)
                    r = result;
                    if (r > 0)
                    {
                        conditionResult = positiveBlock.Eval(context);
                    
                    }
                    else if (negativeBlock != null)
                    {
                        conditionResult = negativeBlock.Eval(context);
                    }
                    return conditionResult;

        }
    }
}
