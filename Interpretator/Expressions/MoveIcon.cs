﻿using System;
using System.Threading;

namespace Interpretator.Expressions
{
    class MoveIcon : BinaryExpression, IFucntionExpression
    {
        public override object Eval(Context context)
        {
            int xPos = Convert.ToInt32(expr1.Eval(context)), yPos = Convert.ToInt32(expr2.Eval(context));
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftDown);
            Thread.Sleep(40);
            MouseEvents.SetCursorPosition(xPos, yPos);
            Thread.Sleep(40);
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.LeftUp);
            return " " + '\b';
        }
    }
}
