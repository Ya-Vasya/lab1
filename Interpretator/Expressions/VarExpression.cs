﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public class VarExpression : UnaryExpression
    {
        public override object Eval(Context context)
        {
            object res = expr1.Eval(context);
            return Convert.ToDouble(res);
        }
    }
}
