﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions
{
    class WhileExpression : ConditionalExpression, IExpression
    {
        public WhileExpression()
        {
            condition = null;
            positiveBlock = null;
        }

        public object Eval(Context context)
        {
            if(positiveBlock is BlockExpression == false)
            {
                throw new Exception("'while' operator needs block expression.");
            }
            bool whileCondition = true;
            object result = null;
            int r = 0;
            while(whileCondition)
            {
                result = condition.Eval(context);
                if (result != null)
                    if (result is int res)
                        r = res;
                if (r > 0)
                {
                    result = positiveBlock.Eval(context);
                }
                else
                    whileCondition = false;
            }
            return result;
        }
    }
}
