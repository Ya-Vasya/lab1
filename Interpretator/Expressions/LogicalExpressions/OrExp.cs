﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions.LogicalExpressions
{
    class OrExp : BinaryExpression
    {
     
public override object Eval(Context context)
        {
            int result = 0;
            object first = expr1.Eval(context);
            if (first != null)
                if (first is double fr)
                    if (fr > 0)
                        return 1;

            object second = expr2.Eval(context);
            if (second != null)
                if (second is double sr)
                    if (sr > 0)
                        result = 1;

            return result;
        }
    }
}
