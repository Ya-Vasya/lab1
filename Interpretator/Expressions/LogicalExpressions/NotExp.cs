﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions.LogicalExpressions
{
    class NotExp : UnaryExpression
    {
        public override object Eval(Context context)
        {
            int result = 1;
            object first = expr1.Eval(context);
            if (first != null)
                if (first is double fr)
                    if (fr > 0)
                        result = 0;
            return result;
        }
    }
}
