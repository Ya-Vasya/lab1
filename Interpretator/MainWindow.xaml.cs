﻿using Interpretator.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Interpretator
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            Tokenizer splitter = new Tokenizer();
            List<List<Token>> tokenList = new List<List<Token>>();
            List<string> lines = new List<string>();
            Context context = new Context();
            Parser parser = new Parser();
            resultField.Text = "";
            string input = inputField.Text;
            //lines = input.Split('\n').ToList();
            lines = input.Split(new string[] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            for (int i = 0; i < lines.Count; i++)
            {
                List<string> tokens = splitter.tokenize(lines[i]);
                splitter.Analyze(ref tokenList, tokens);
                parser.formOperators(tokenList[i]);
                IExpression root = parser.buildTree();
                if (root is ConditionalExpression ie)
                {
                    CheckRootForIf(ie, context, ref i, lines, parser);
                }
                resultField.Text += root.Eval(context).ToString() + "\n";
            }
        }

        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.RightShift)
            {
                RunButton_Click(sender, e);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.KeyDown += new KeyEventHandler(MainWindow_KeyDown);
        }

        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.RightShift)
            {
                RunButton_Click(sender, e);
            }
        }

        private void CheckRootForIf(ConditionalExpression root, Context context, ref int i, List<string> lines, Parser parser)
        {
            Tokenizer innerTok = new Tokenizer();
            List<string> innerList = new List<string>();
            List<List<Token>> innerTokenList = new List<List<Token>>();
            BlockExpression blockExpression = new BlockExpression();
            if (root is IfExpression)
            {
                i++;
                if (lines[i][0] == '{')
                {
                    i++;
                    int j = 0;
                    while (lines[i][0] != '}')
                    {
                        innerList = innerTok.tokenize(lines[i]);
                        innerTok.Analyze(ref innerTokenList, innerList);
                        parser.formOperators(innerTokenList[j]);
                        IExpression innerExpr = parser.buildTree();
                        if (innerExpr is ConditionalExpression ce)
                        {
                            CheckRootForIf(ce, context, ref i, lines, parser);
                        }
                        blockExpression.addToList(innerExpr);
                        j++;
                        i++;
                    }
                    root.positiveBlock = blockExpression;
                }
            }
                if (root is IfExpression ie)
                {
                    if (lines.Count > i + 1)
                        i++;
                    if (lines[i].TrimEnd('\r', '\n') == "else")
                    {
                        i++;
                        BlockExpression ibe = new BlockExpression();
                        if (lines[i][0] == '{')
                        {
                            i++;
                            int k = 0;
                            while (lines[i][0] != '}')
                            {
                                innerList = innerTok.tokenize(lines[i]);
                                innerTokenList.Clear();
                                innerTok.Analyze(ref innerTokenList, innerList);
                                parser.formOperators(innerTokenList[k]);
                                IExpression innerExpr = parser.buildTree();
                                if (innerExpr is ConditionalExpression ce)
                                {
                                    CheckRootForIf(ce, context, ref i, lines, parser);
                                }
                                ibe.addToList(innerExpr);
                                k++;
                                i++;
                            }
                            ie.negativeBlock = ibe;
                        }
                    }
                }
                if (root is WhileExpression we)
                {
                BlockExpression wbe = new BlockExpression();
                    i++;
                    if (lines[i][0] == '{')
                    {
                        i++;
                        int j = 0;
                        while (lines[i][0] != '}')
                        {
                            innerList = innerTok.tokenize(lines[i]);
                            innerTok.Analyze(ref innerTokenList, innerList);
                            parser.formOperators(innerTokenList[j]);
                            IExpression innerExpr = parser.buildTree();
                            if (innerExpr is ConditionalExpression ce)
                            {
                                CheckRootForIf(ce, context, ref i, lines, parser);
                            }
                            wbe.addToList(innerExpr);
                            j++;
                            i++;
                        }
                        root.positiveBlock = wbe;
                    }

                }
        }
    }
}
