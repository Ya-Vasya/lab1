﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator
{
    public class Context
    {
        Dictionary<string, double> variables;

        public Context()
        {
            variables = new Dictionary<string, double>();
        }

        public object GetVariable(string name)
        {
            if (variables.ContainsKey(name))
                return variables[name];
            else
                return 0;
        }

        public void SetVariable(string name, double value)
        {
            if (variables.ContainsKey(name))
            {
                variables[name] = value;
            }
            else
                variables.Add(name, value);
        }


    }
}
